# hydrabox_gnss_driver

This is [ROS](https://www.ros.org) package with driver for hydrabox gnss module.

## launching

There are two example launch file provided:

* hydrabox.launch
* imutools.launch

Second one uses [imu_tools](http://wiki.ros.org/imu_tools) for calculating
roll pitch and yaw from raw imu values. This should be better than build in
calculations that are used in first script.

Configuration:

* port - Port to connect to. Default 5555 should be fine.
* host - Ip address of hydrabox. Default is 127.0.0.1, override in launch files.
* frame - Frame name for sensor placement. Default is gps.
* validate - Build in message validation can be disabled (to save computing power).

On topic gps/heading heading estimation from GNSS is published. This is normally
done based on movement, but when two boxes are used in moving baseline mode
provide differential heading.

## tests

This package can be tested by nosetests/unittests packages.
Syntax is checked with flake8.

```sh
nosetests
```

It is integrated with catkin:

```sh
catkin_make run_tests
```
