#!/usr/bin/env python
import rospy
from hydrabox_gnss_driver.hydrabox_client_ros import HydraboxClientRos


if __name__ == '__main__':
    try:
        hc = HydraboxClientRos()
        hc.run()
    except rospy.ROSInterruptException:
        pass
