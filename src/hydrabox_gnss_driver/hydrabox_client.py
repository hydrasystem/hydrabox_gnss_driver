#!/usr/bin/env python

import time
import socket
import json
import logging
from jsonschema import validate

SCHEMA_GENERAL = {
    "type": "object",
    "properties": {
        "type": {
            "type": "string",
            "enum": ["imu", "gps"]
        },
        "data": {
            "type": "object"
        }
    },
    "required": ["type", "data"]
}

SCHEMA_IMU_RAW = {
    "$schema'": "http://json-schema.org/schema#",
    "$id'": "http://dev.hydrasystem.pl/schemas/imu_raw.schema.json",
    "type": "object",
    "properties": {
        "acc": {
            "type": "array",
            "items": {"type": "number"},
            "minItems": 3,
            "maxItems": 3
        },
        "gyro": {
            "type": "array",
            "items": {"type": "number"},
            "minItems": 3,
            "maxItems": 3
        },
        "mag": {
            "type": "array",
            "items": {"type": "number"},
            "minItems": 3,
            "maxItems": 3
        },
        "quat": {
            "type": "array",
            "items": {"type": "number"},
            "minItems": 4,
            "maxItems": 4
        },
        "rpy": {
            "type": "array",
            "items": {"type": "number"},
            "minItems": 3,
            "maxItems": 3
        },
        "ts": {"type": "number"}
    },
    "required": ["acc", "gyro", "mag", "quat", "rpy", "ts"]
}

SCHEMA_GNSS_RAW = {
    "$schema'": "http://json-schema.org/schema#",
    "$id'": "http://dev.hydrasystem.pl/schemas/gnss_raw.schema.json",
    "type": "object",
    "properties": {
        "status": {
            "properties": {
                "pDOP": {"type": "number"},
                "positionLatLonAlt": {
                    "type": "array",
                    "items": {"type": "number"},
                    "minItems": 3,
                    "maxItems": 3
                },
                "satellites_dB": {"type": "string"},
                "gga": {"type": "string"},
                "vAcc": {"type": "number"},
                "position": {
                    "type": "array",
                    "items": {"type": "number"},
                    "minItems": 3,
                    "maxItems": 3
                },
                "estAccuracy": {"type": "number"},
                "heading": {"type": "number"},
                "headingAccuracy": {"type": "number"},
                "hAcc": {"type": "number"}
            },
            "required": [
                "pDOP",
                "positionLatLonAlt",
                "satellites_dB",
                "gga",
                "vAcc",
                "position",
                "estAccuracy",
                "heading",
                "headingAccuracy",
                "hAcc"
            ]
        },
        "ts": {"type": "number"},
        "general": {
            "type": "object",
            "properties": {
                "mode": {
                    "type": "string"
                },
                "gpsFix": {
                    "type": "string"
                }
            }
        }
    },
    "required": ["status", "general", "ts"]
}


class HydraboxClient(object):
    def __init__(self, host='127.0.0.1', port=5555):
        # We send json separated by newline like defined here.
        self.separator = '\r\n'
        self.host = host
        self.timeout = 5
        self.port = port
        self.validate = True
        self.socket = None

    def get_single_json(self, raw):
        data = None
        raw_lines = raw.strip().split(self.separator)
        if len(raw_lines) == 0:
            # Nothing to parse
            return None, raw
        elif len(raw_lines) == 1:
            if len(raw_lines[0]) == 0:
                return None, raw
        try:
            data = json.loads(raw_lines[0])
        except ValueError:
            # In this case we don't have full json yet. Probably rest will be
            # received later, so everything is fine.
            return None, raw
        # Remove from law data processed data length + separator.
        raw = raw[len(raw_lines[0])+len(self.separator):]
        return data, raw

    def running_condition(self):
        return True

    def run(self):
        hydra_connected = False

        while (not hydra_connected) and self.running_condition():
            try:
                logging.info('Connecting to %s:%d' % (self.host, self.port))
                self.socket = socket.create_connection(
                    (self.host, self.port), timeout=self.timeout)
                hydra_connected = True
            except socket.error as error:
                logging.warning(
                    'Unable to connect to hydrabox, will retry in some time')
                logging.warning(error)
                time.sleep(self.timeout)
                continue
        raw = ''
        while self.running_condition():
            raw += self.socket.recv(1024)
            while True:
                item, raw = self.get_single_json(raw)
                if item is not None:
                    self.parse_data(item)
                else:
                    break

    def parse_data(self, data):
        if self.validate:
            validate(data, schema=SCHEMA_GENERAL)
        if data['type'] == 'imu':
            if self.validate:
                validate(data['data'], schema=SCHEMA_IMU_RAW)
            self.parse_imu(data['data'])
        elif data['type'] == 'gps':
            if self.validate:
                validate(data['data'], schema=SCHEMA_GNSS_RAW)
            self.parse_gps(data['data'])
        else:
            logging.warning('Got message with no type')

    def parse_imu(self, data):
        pass

    def parse_gps(self, data):
        pass


if __name__ == '__main__':
    hydra_client = HydraboxClient()
    hydra_client.run()
