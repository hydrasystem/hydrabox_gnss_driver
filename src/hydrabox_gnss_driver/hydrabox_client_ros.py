#!/usr/bin/env python

from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import (super)

import rospy
import math
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from sensor_msgs.msg import NavSatFix
from sensor_msgs.msg import NavSatStatus
import tf

from hydrabox_gnss_driver.hydrabox_client import HydraboxClient


class HydraboxClientRos(HydraboxClient):
    def __init__(self):
        super().__init__()
        rospy.init_node('hydrabox_gnss_driver')
        self.host = rospy.get_param("~host", default="127.0.0.1")
        rospy.set_param("~host", self.host)
        self.port = int(rospy.get_param("~port", default=5555))
        rospy.set_param("~port", self.port)
        self.frame = rospy.get_param("~frame", default="gps")
        rospy.set_param("~frame", self.frame)
        self.validate = bool(rospy.get_param("~validate", default="true"))
        rospy.set_param("~validate", self.validate)
        self.nav_sat_fix_pub = rospy.Publisher(
            'gps/data', NavSatFix, queue_size=10)
        self.imu_pub = rospy.Publisher('imu/data', Imu, queue_size=10)
        self.magnetic_field_pub = rospy.Publisher(
            'imu/mag', MagneticField, queue_size=10)
        self.heading_pub = rospy.Publisher('gps/heading', Imu, queue_size=10)

    def running_condition(self):
        return not rospy.is_shutdown()

    def parse_imu(self, data):
        # {
        #  'acc': [0.195896, -0.1821, 9.45071],
        #  'gyro': [0.00888, 0.01252, -0.0215],
        #  'mag': [-0.00010, -0.0001, -6.5626e-05],
        #  'quat': [0.31689, -0.01368, 0.0027, -0.94835],
        #  'rpy': [-2.79578, -1.38776, -143.03599],
        #  'ts': 6539060
        # }
        msg = Imu()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = self.frame
        msg.linear_acceleration_covariance = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        msg.angular_velocity_covariance = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        msg.orientation_covariance = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        mag_msg = MagneticField()
        mag_msg.header.stamp = rospy.Time.now()
        mag_msg.header.frame_id = self.frame
        mag_msg.magnetic_field_covariance = [0, 0, 0, 0, 0, 0, 0, 0, 0]

        try:

            msg.angular_velocity.x = data['gyro'][0]
            msg.angular_velocity.y = data['gyro'][1]
            msg.angular_velocity.z = data['gyro'][2]
            msg.angular_velocity_covariance[0] = 0.02
            msg.angular_velocity_covariance[4] = 0.02
            msg.angular_velocity_covariance[8] = 0.02
            msg.linear_acceleration.x = data['acc'][0]
            msg.linear_acceleration.y = data['acc'][1]
            msg.linear_acceleration.z = data['acc'][2]
            msg.linear_acceleration_covariance[0] = 0.04
            msg.linear_acceleration_covariance[4] = 0.04
            msg.linear_acceleration_covariance[8] = 0.04
            msg.orientation.x = data['quat'][0]
            msg.orientation.y = data['quat'][1]
            msg.orientation.z = data['quat'][2]
            msg.orientation.w = data['quat'][3]
            msg.orientation_covariance[0] = 0.0025
            msg.orientation_covariance[4] = 0.0025
            msg.orientation_covariance[8] = 0.0025
            mag_msg.magnetic_field.x = data['mag'][0]
            mag_msg.magnetic_field.y = data['mag'][1]
            mag_msg.magnetic_field.z = data['mag'][2]
            mag_msg.magnetic_field_covariance[0] = 0.0
            mag_msg.magnetic_field_covariance[4] = 0.0
            mag_msg.magnetic_field_covariance[8] = 0.0
        except KeyError as e:
            rospy.logwarn('Unable to IMU parse message ' + str(e))
            print(data)
        except ValueError as e:
            rospy.logwarn('Unable to IMU parse message ' + str(e))
            print(data)
        else:
            # No exception
            self.imu_pub.publish(msg)
            self.magnetic_field_pub.publish(mag_msg)

    def parse_gps(self, data):
        # {
        #   u'status':
        #   {
        #       u'pDOP': 99.99,
        #       u'positionLatLonAlt': [u'0.0000', u'0.00000', u'0.0000'],
        #       u'satellites_dB': u'15,',
        #       u'gga': u'',
        #       u'vAcc': 3750111.232,
        #       u'position': [u'6378137.0000', u'0.0000', u'0.0000'],
        #       u'estAccuracy': 4294967.295, u'heading': 0,
        #       u'headingAccuracy': 180,
        #       u'hAcc': 4294967.295
        #   },
        #   u'ts': 77168472,
        #   u'general':
        #   {
        #       u'gpsFix': u'no fix/invalid',
        #       u'mode': u'rover'
        #   }
        # }
        fix = NavSatFix()
        heading = Imu()
        heading.header.stamp = rospy.Time.now()
        heading.header.frame_id = self.frame
        fix.header.stamp = rospy.Time.now()
        fix.header.frame_id = self.frame
        fix.status.service = NavSatStatus.SERVICE_GPS
        fix.position_covariance = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        fix.position_covariance_type = NavSatFix.COVARIANCE_TYPE_APPROXIMATED
        try:
            fix.latitude = float(data['status']['positionLatLonAlt'][0])
            fix.longitude = float(data['status']['positionLatLonAlt'][1])
            fix.altitude = float(data['status']['positionLatLonAlt'][2])
            fix.position_covariance[0] = float(data['status']['hAcc'])
            fix.position_covariance[4] = float(data['status']['hAcc'])
            fix.position_covariance[8] = float(data['status']['vAcc'])
            if data['general']['fix'] == 'no fix/invalid':
                fix.status.status = NavSatStatus.STATUS_NO_FIX
            else:
                fix.status.status = NavSatStatus.STATUS_FIX
            yaw = float(data['status']['heading'])
            # Yaw is originally in degrees, zero when crossing north, increases
            # clockwise. According to REP-103, we want 0 east, and increasing
            # counter clockwise
            # So north 0 -> 90
            # East 90 -> 0
            # West -90 -> 180
            # South 180 or -180 -> -90
            yaw = -yaw + 90
            yaw = math.radians(yaw)
            yaw_error = float(math.radians(data['status']['headingAccuracy']))
            quat = tf.transformations.quaternion_from_euler(0, 0, yaw)
            heading.orientation.x = quat[0]
            heading.orientation.y = quat[1]
            heading.orientation.z = quat[2]
            heading.orientation.w = quat[3]
            heading.orientation_covariance = [
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
            heading.orientation_covariance[8] = yaw_error

        except KeyError as e:
            rospy.logwarn('Unable to GNSS parse message ' + str(e))
            rospy.logwarn(data)
        else:
            self.heading_pub.publish(heading)
            self.nav_sat_fix_pub.publish(fix)


if __name__ == '__main__':
    try:
        hc = HydraboxClientRos()
        hc.run()
    except rospy.ROSInterruptException:
        pass
