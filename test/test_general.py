import unittest
import subprocess


class TestGeneral(unittest.TestCase):
    def test_flake8(self):
        """Testing the retval of flake8 command to be zero (PEP8 check)."""
        p = subprocess.Popen(['python', '-m', 'flake8'])
        retval = p.wait()
        self.assertEqual(retval, 0, 'Test flake8 linter')
