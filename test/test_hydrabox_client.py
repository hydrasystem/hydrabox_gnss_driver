import unittest
from jsonschema import ValidationError
from hydrabox_gnss_driver.hydrabox_client import HydraboxClient


class TestUM(unittest.TestCase):

    def setUp(self):
        pass

    def test_init(self):
        _ = HydraboxClient()

    def test_get_single_json(self):
        hc = HydraboxClient()
        raw = '{}\r\n{}'
        item, raw = hc.get_single_json(raw)
        self.assertEqual(raw, '{}')
        self.assertEqual(item, {})
        item, raw = hc.get_single_json(raw)
        self.assertEqual(raw, '')
        self.assertEqual(item, {})

    def test_parse_general(self):
        hc = HydraboxClient()
        with self.assertRaises(ValidationError):
            hc.parse_data({})
        with self.assertRaises(ValidationError):
            hc.parse_data({"type": "test", "data": {}})
        # This should not raise now.
        hc.parse_data({"type": "imu", "data": {
            'acc': [
                0.19589658081531525,
                -0.18210914731025696,
                9.45071792602539
            ],
            'gyro': [
                0.008880757726728916,
                0.012528236024081707,
                -0.021537888795137405
            ],
            'mag': [
                -0.00010609201126499102,
                -0.00013743649469688535,
                -6.562667840626091e-05
            ],
            'quat': [
                0.31689611077308655,
                -0.01368665136396885,
                0.002746865153312683,
                -0.9483575224876404
            ],
            'rpy': [
                -2.795783042907715,
                -1.3877670764923096,
                -143.03599548339844
            ],
            'ts': 6539060
        }
        })
        hc.parse_data({"type": "gps", "data": {
            u'status':
            {
                'pDOP': 99.99,
                'positionLatLonAlt': [
                    0.000000000,
                    0.000000000,
                    0.000000000
                ],
                'satellites_dB': u'15,',
                'gga': u'',
                'vAcc': 3750111.232,
                'position': [
                    6378137.0000,
                    0.0000,
                    0.0000
                ],
                'estAccuracy': 4294967.295,
                u'heading': 0,
                'headingAccuracy': 180,
                'hAcc': 4294967.295
            },
            u'ts': 77168472,
            u'general':
            {
                u'gpsFix': u'no fix/invalid',
                u'mode': u'rover'
            }
        }})


if __name__ == '__main__':
    unittest.main()
